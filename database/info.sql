/**Table 1 -- Create an Entry**/
CREATE TABLE IF NOT EXISTS entry (
    entryID VARCHAR(255) NOT NULL PRIMARY KEY,
    entryName VARCHAR(255) NOT NULL DEFAULT '',
    entryEmail VARCHAR(255) NOT NULL DEFAULT '',
    entryPhoneNumber VARCHAR(11) NOT NULL DEFAULT '',
    entryContent VARCHAR(255) NOT NULL DEFAULT ''
);

/**Table 1 - Values -- Create an Entry**/
INSERT INTO entry VALUES
("16a8a49d-186f-42c8-97de-380b27c6d3eb","Steve Austin","SteveAustin@gmail.com","6472392323", "Hey its' Austin from Global Tech, please call me"),
("719cca8d-1f1d-4915-bc7e-2207e650169e","Chris Jericho", "chrisjericho@gmail.com","9050443434", "Hey Jonathan please call us regarding the job"),
("937de25a-06ea-43cb-9bf0-770dd271a44e","Chris Paul", "chrispaul@gmail.com","9052444434", "Hey Its Chris from Forsys Software , please call me regarding the job"),
("c8fe43a2-d9d8-4762-8b8f-21a920206c3b","Jessica Mony", "jessicam@gmail.com","6372454434", "Jon, Its Jessica from Moneris please call us back");





/**Table 2 -- Create an User**/
CREATE TABLE IF NOT EXISTS user (
    userID VARCHAR(255) NOT NULL PRIMARY KEY,
    userName VARCHAR(255) NOT NULL DEFAULT '',
    userEmail VARCHAR(255) NOT NULL DEFAULT '',
    userPassword VARCHAR(255) NOT NULL DEFAULT ''
);

/**Table 2 - VALUES -- Create an User**/
INSERT INTO user VALUES
("ed31ac23-ded3-4a35-bbb4-5e7763c3bc0a","Steve Austin","SteveAustin@gmail.com","$2b$10$j95PSsfwVDZL58qKm6QKt.TFhePBBXdHXyAjJsSVzlK5cV00rDsxa"),
("f3ab1eee-57c3-4a29-99c9-6fdc52fa280f","Chris Jericho", "chrisjericho@gmail.com","$2b$10$lyW/d6T.bZoEgmTgAvWUCesF2YhVVCCWHDpWth/s4nKU7ue1WBGbW"),
("fad4717e-eb99-488d-9b9c-b78167040852","Chris Paul", "chrispaul@gmail.com","$2b$10$7J3jEAVKKWq2Ou1RXNMWTuq3W1JIvvNecewwJFMrjBSi5Tbk9z4FW"),
("ca0cadc7-bb4d-4430-9ae3-42bc53622ef0","Jessica Mony", "jessicam@gmail.com","$2b$10$cvNVQooHW22ruWwfvHB0NOtWVQMJKlevcJYw90HkXXsAmLcaqUDrO");





/**Table 3 -- ResumeFeedback/Comments**/ 
CREATE TABLE IF NOT EXISTS resume (
    resumeEmployerID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    resumeEmployerName VARCHAR(255) NOT NULL DEFAULT '',
    resumeEmployerContact VARCHAR(255) NOT NULL DEFAULT '',
    resumeEmployerOpening VARCHAR(255) NOT NULL DEFAULT '',
    resumeEmployerDate Date NOT NULL,
    resumeEmployerFeedback VARCHAR(255) NOT NULL DEFAULT '',
    entryID VARCHAR(255) NOT NULL ,
    FOREIGN KEY (entryID) REFERENCES entry(entryID) ON DELETE CASCADE,
    PRIMARY KEY (resumeEmployerID)
);


/**Table 3 - Values -- ResumeFeedback/Comments**/
INSERT INTO resume VALUES
(1,"Steve Austin","My email is SteveAustin@gmail.com from Global Tech, please contact our team today regarind the job","There is an opening for a Junior Develper","20210713", "We enjoyed your resume","16a8a49d-186f-42c8-97de-380b27c6d3eb"),
(2,"Chris Jericho", "My email is chrisjericho@gmail.com calling from Forsys Tech, please contact us today","There is an opening for a Junior Develper", "20210802","Loved your Work here","719cca8d-1f1d-4915-bc7e-2207e650169e" ),
(3,"Chris Paul", "My office hours are 9am to 5pm please reach me at : chrispaul@gmail.com","We have a developer position open for you","20210802","Nice Resume & Cover Letter","937de25a-06ea-43cb-9bf0-770dd271a44e" ),
(4,"Jessica Mony", "My Nmae is Jessica from YMCA Tech, my email is jessicam@gmail.com","Would you be interested in a Junior Developer Role?","20210803","Went through your work & we are impressed","c8fe43a2-d9d8-4762-8b8f-21a920206c3b");






/**Table 4 -- Portfolio Feedback/Rating**/
CREATE TABLE IF NOT EXISTS portfolio (
    portfolioEmployerID INT UNSIGNED NOT NULL AUTO_INCREMENT,
    portfolioEmployerContact VARCHAR(255) NOT NULL DEFAULT '',
    portfolioEmployerFeedback VARCHAR(255) NOT NULL DEFAULT '',
    portfolioEmployerRating VARCHAR(255) NOT NULL DEFAULT '',
    entryID VARCHAR(255) NOT NULL ,
    FOREIGN KEY (entryID) REFERENCES entry(entryID) ON DELETE CASCADE,
    PRIMARY KEY (portfolioEmployerID)
);

/**Table 4 -- Values -- Portfolio Feedback/Rating*/
INSERT INTO portfolio VALUES
(1,"Hey its Steve Austin, my email is SteveAustin@gmail.com from Global Tech, please contact our team today regarind the job","There is an opening for a Junior Develper as mentioned & we think you would like the role here","We enjoyed your portfolio here would rate it a 7/10","16a8a49d-186f-42c8-97de-380b27c6d3eb"),
(2,"Its Chris Jericho from Forsys, my email is chrisjericho@gmail.com calling from Forsys Tech, please contact us today","There is an opening for a Junior  & we think with your work we would like to interview you", "Portfolio Looks clean !","719cca8d-1f1d-4915-bc7e-2207e650169e" ),
(3,"Its Chris Paul again a reminder, my office hours are 9am to 5pm please reach me at : chrispaul@gmail.com","We have a developer position open for you and we are impressed with what we have seen","We could help you polish your portfolio, although Great work","937de25a-06ea-43cb-9bf0-770dd271a44e" ),
(4,"Jessica from YMCA Tech, my email is jessicam@gmail.com","I think your Portfolio looked Great could you use some improvements but well done ","I give this a B+ , could be more polished with room for improvement","c8fe43a2-d9d8-4762-8b8f-21a920206c3b");



