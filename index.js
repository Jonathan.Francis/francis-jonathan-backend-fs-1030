import dotenv from "dotenv"
import express from 'express'
import cors from 'cors'
import { v4 as uuidv4 } from 'uuid';
import validateEntries from "./src/middleware/validateEntries";
import validateUser from "./src/middleware/validateUser";
import verifyToken from "./src/middleware/jwtVerify";
import db from "./database/connection";
import resumeRoutes from "./src/routes/resume";
import portfolioRoutes from "./src/routes/portfolio";

dotenv.config()

const app = express()
const port = process.env.PORT || 8080
const { body, validationResult } = require('express-validator');

let jwt = require('jsonwebtoken');
const bcrypt = require("bcrypt");
const saltRounds=10;
let privateKey="shhhhh"

// allows us to parse json 
app.use(express.urlencoded({ extended: false }))
app.use(express.json())
app.use(cors())
app.use('/resume',resumeRoutes)
app.use('/portfolio',portfolioRoutes)



//*Hello World

app.get("/", (req, res) =>{
  res.send("Hello World");
});


//*CREATE ENTRY PAGE - Create Entries
app.post("/contact_form/entries", validateEntries, body('name').notEmpty(),body('email').isEmail(),body('phoneNumber').isMobilePhone(),body('content').notEmpty(),(req,res) => {
    let newEntries = {
        id: uuidv4(),
        name: req.body.name,
        email: req.body.email,
        phoneNumber : req.body.phoneNumber,
        content: req.body.content
}
 
const errors = validationResult(req);

if (!errors.isEmpty()) {
      const invalidProperties = [];
       errors.array().forEach(a => {
        invalidProperties.push(a.param);
});
    return res.status(400).send({
           message: 'validation error',
              invalid: [`${invalidProperties}`],
});
}


  db.query(
     "INSERT INTO entry (entryID,entryName,entryEmail,entryPhoneNumber, entryContent) VALUES (?, ?, ?, ?, ?)",
          [newEntries.id,newEntries.name,newEntries.email,newEntries.phoneNumber,newEntries.content],
              function (error, results, fields) {
                  if (error) throw error;
                      return res.status(201).send({message:"Successful Entry"});
            }
     );
})





//*CREATE USER PAGE - Create Users
app.post("/users", validateUser,body('name').notEmpty(),body('password').isLength({ min: 8 }),body('email').isEmail(),(req,res) =>{
  let newUser = {
      id: uuidv4(),
      name: req.body.name,
      email: req.body.email,
      password: req.body.password
}

  const errors = validationResult(req);

    if(!errors.isEmpty()){
      const missingValues = [];
          errors.array().forEach(a=>{
                missingValues.push(a.param)
          })
            return res.status(400).send({
              message: 'validation error',
                 invalid: [`${missingValues}`],
  });
}   

    bcrypt.hash(newUser.password, saltRounds, function (err, hash) { 
    newUser.password = hash;
   
        db.query(
             "INSERT INTO user (userID,userName,userEmail,userPassword) VALUES (?, ?, ?, ?)",
                [newUser.id,newUser.name,newUser.email,newUser.password],
                   function (error, results, fields) {
                      if (error) throw error;
                        return res.status(201).send({message:"Successful Entry"});
            }
        );
    })
})




///*LOGIN PAGE - Registered User gets Token   
app.post('/auth', (req, res) => {
  let registeredUser = {
    email: req.body.email,
    password: req.body.password,
};


  db.query(
    'SELECT * FROM `user` WHERE `userEmail` = ?', [registeredUser.email],
          function (error, results, fields) {
              if (error) throw error;
              console.log('Results', results)
              console.log('fields', fields)

                const hashedPassword = results[0].userPassword

                  bcrypt.compare(registeredUser.password, hashedPassword, function(err, result) {

                    if (result) {
                       const token = jwt.sign(registeredUser, privateKey)
                        return res.status(201).send(token);
                  }

                          return res.status(401).send({
                            message: 'incorrect credentials provided',
                });
            });
          }
  );
});




//**SUBMISSIONS PAGE - Registered User with Token gets access to Entire Entries Array
app.get("/contact_form/entries", verifyToken, (req,res) => {
  db.query(
    `SELECT * FROM entry`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
      }
    );
})



//SUBMISSION PROFILE PAGE - View Individual Entry at Specific ID
app.get("/contact_form/entries/:id",(req,res)=>{

  db.query(
    `SELECT * FROM entry WHERE entryID="${req.params.id}"`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
      }
    );
})



//SUBMISSION PROFILE PAGE - Delete an Individual Entry at a Specific ID
app.delete("/contact_form/entries/:id",(req,res) => {
  db.query(
    `DELETE FROM entry WHERE entryID="${req.params.id}"`,
    function (error, results, fields) {
      if (error) throw error;
      return res.status(200).send(results);
    }
  );
});


//SUBMISSION PROFILE PAGE - Update an Individual Entry at a Specific ID
app.put("/contact_form/entries/:id",(req,res) => {
    const { entryName, entryPhoneNumber, entryContent } = req.body;
       db.query(
        `UPDATE entry SET entryName="${entryName}",entryPhoneNumber="${entryPhoneNumber}", entryContent="${entryContent}" WHERE entryID="${req.params.id}"`,
            function (error, results, fields) {
              if (error) throw error;
                return res.status(200).send(results);
    }
  );
});


app.listen(port, () => {
  console.log(`Example app listening at http://localhost:${port}`)
})