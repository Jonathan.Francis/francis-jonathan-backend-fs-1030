import express from "express"
import db from "../../database/connection.js"

const router = express.Router()


/**Resume Feedback Form Page - Submit a Feedback**/
router.post("/", (req, res) => {
    db.query(
      "INSERT INTO resume (resumeEmployerName, resumeEmployerContact, resumeEmployerOpening, resumeEmployerDate, resumeEmployerFeedback, entryID) VALUES (?,?,?,?,?,?)",
      [req.body.resumeEmployerName, req.body.resumeEmployerContact, req.body.resumeEmployerOpening,req.body.resumeEmployerDate, req.body.resumeEmployerFeedback, req.body.entryID],
      function (error, results, fields) {
        if (error) throw error;
        return res.status(201).send(results);
      }
    );
  });
  
  /**Resume Feedback Form List Page - View All Feedbacks**/
   router.get("/", (req, res) => {
      db.query(
        "SELECT * FROM resume",
        function (error, results, fields) {
          if (error) throw error;
          return res.status(200).send(results);
        }
      );
  });
  

  /**Resume Feedback Form List Profile Page - View Indiviual Feedback**/
  router.get("/:id", (req, res) => {
    db.query(
      `SELECT * FROM resume WHERE resumeEmployerID=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  
  
  /**Resume Feedback Form List Profile Page - DELETE FEEDBACK - Delete Indiviual Feedback**/
  router.delete("/:id", (req, res) => {
    db.query(
      `DELETE FROM resume WHERE resumeEmployerID=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  
  
  /**Resume Feedback Form List Profile Page - EDIT FEEDBACK - Edit Indiviual Feedback */
  router.put("/:id", (req, res) => {
    const {resumeEmployerName, resumeEmployerContact, resumeEmployerOpening,resumeEmployerFeedback} = req.body;
    db.query(
      `UPDATE resume SET resumeEmployerName="${resumeEmployerName}",resumeEmployerContact="${resumeEmployerContact}",resumeEmployerOpening="${resumeEmployerOpening}",resumeEmployerFeedback="${resumeEmployerFeedback}" WHERE resumeEmployerID="${req.params.id}"`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  
  export default router;
  
  