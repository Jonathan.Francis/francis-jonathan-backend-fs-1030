import express from "express"
import db from "../../database/connection.js"

const router = express.Router()


/**Portfolio Feedback Form Page - Submit a Feedback**/
router.post("/", (req, res) => {
    db.query(
      "INSERT INTO portfolio (portfolioEmployerContact, portfolioEmployerFeedback, portfolioEmployerRating, entryID) VALUES (?,?,?,?)",
      [req.body.portfolioEmployerContact, req.body.portfolioEmployerFeedback, req.body.portfolioEmployerRating,req.body.entryID],
      function (error, results, fields) {
        if (error) throw error;
        return res.status(201).send(results);
      }
    );
  });
  
/**Portfolio Feedback Form List Page - View All Feedbacks**/
   router.get("/", (req, res) => {
      db.query(
        "SELECT * FROM portfolio",
        function (error, results, fields) {
          if (error) throw error;
          return res.status(200).send(results);
        }
      );
  });
  

/**Portfolio Feedback List Profile Page - View Indiviual Feedback**/
  router.get("/:id", (req, res) => {
    db.query(
      `SELECT * FROM portfolio WHERE portfolioEmployerID=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  
  
/**Portfolio Feedback List Profile Page - DELETE FEEDBACK - Delete Indiviual Feedback**/
  router.delete("/:id", (req, res) => {
    db.query(
      `DELETE FROM portfolio WHERE portfolioEmployerID=${req.params.id}`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  
/**Porfolio Feedback List Profile Page - EDIT FEEDBACK - Edit Indiviual Feedback */
  router.put("/:id", (req, res) => {
    const {portfolioEmployerContact, portfolioEmployerFeedback, portfolioEmployerRating} = req.body;
    db.query(
      `UPDATE portfolio SET portfolioEmployerContact="${portfolioEmployerContact}",portfolioEmployerFeedback="${portfolioEmployerFeedback}",portfolioEmployerRating="${portfolioEmployerRating}" WHERE portfolioEmployerID="${req.params.id}"`,
      function (error, results, fields) {
        if (error) throw error;
        return res.status(200).send(results);
      }
    );
  });
  
  export default router;
  
  