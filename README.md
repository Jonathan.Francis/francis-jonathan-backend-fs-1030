W21 FS1030 – Jonathan Francis -- PERSONAL PORTFOLIO PROJECT BACKEND


# Links below - for the W21 FS1030 – Jonathan Francis -- PERSONAL PORTFOLIO PROJECT :

*Active - For the Backend API - https://gitlab.com/Jonathan.Francis/francis-jonathan-backend-fs-1030

*Active - For the Frontend - https://gitlab.com/jonathan-francis/francis_jonathan_frontend_fs1030_localhost

*Active - For the Database - https://gitlab.com/Jonathan.Francis/francis-jonathan-database-fs-1030

Inactive - For the Frontend used in the York Full Stack Program during the Free GCP Trial. This is no longer active here for reference only.
https://gitlab.com/Jonathan.Francis/francis-jonathan-frontend-fs-1030


# Backend API
The Backend API can be started on either Node.js or Docker.
A simple and basic CRUD application (Create, Read, Update, Delete) using Node.js, Express, OR Docker & MySQL.


# Information about the .env file

1. This project uses the .env file to store important information for this application
2. Rename the .envexample to .env and adjust the properties in this file accordingly
3. PORT refers to the port this application will use on your system
4. It is important to note that the Docker & npm use different .env instances so be sure to select the appropriate one when using either Docker or npm.

# Run this project using npm

1. To run this project you will need to clone this repo using SSH or HTTP
2. Open the folder in VS Code or applicable source-code editor with access to terminal
3. When in the terminal, use npm install to install node dependencies
4. The project uses http://localhost:8080 to run so ensure that port 8080 is not in use (you can change the port by going into .env and adjusting the variable of port from 8080 to something else)
5. In the terminal you can type in npm run start to start this project

# Using Docker to run this project

1. You can run this project using docker compose
2. Clone the repo using SSH or HTTP
3. Ensure that Docker is installed
4. Use the following command to start this application:

a) docker-compose build
b) docker-compose up

5. This should start both the API and the MySQL server required for this project
6. Connect to the SQL server and run the personal.sql file in your preferred SQL administration software.(Import the personal.sql file into MySQL Workbench). This sql file can be found in this link - https://gitlab.com/Jonathan.Francis/francis-jonathan-database-fs-1030. 


# Information on connecting the backend and database

Follow the Instructions from the following link - below - to get the two connected - whether using npm or Docker with MySQL.
https://gitlab.com/Jonathan.Francis/francis-jonathan-database-fs-1030

# Special Instructions For this Project and Details to Use Website.

1) As an Employer, you must leave an Entry on the Entry Page.
2) Once completed, you can use the same credientals to start/create a user account on the User Page but must have a unique 8 character mininum password.
3) After creating an user account, use the Email & Password to Login on the Login Page.
4) Once Logged in you must make sure to remember your unique Employer Entry ID.
5) To CRUD Employer Feedbacks/Comments on the Resume/Portfolio Pages you will require your Employer Entry ID as it is a foreign key.

Please note the following details::::

To create a user, make a POST request to /users. 
Request must contain name, email (must be valid email), password (mininum 8 characters).
To validate a user login, make a POST request to /auth. Request must contain email and password.

Thank you!

# Extra details localhost:3000 & GCP WEBSITE

http://localhost:3000/

Initially, this project was accessible on GCP (Google Console Platform) during the duration of my enrollment in the York University Continuing Education Program for the FULL STACK WEB DEVELOPMENT Course between Nov - Dec 2021. We were provided with a free trial to have our platform uploaded on GCP for a certian amount of days - this link http://personal-project.jonathan-francis.me/ is no longer accessible due to the GCP free trial expiring in 2021. Instead, the Front End will be running on - http://localhost:3000/. Refer to .gitlab-ci.yml file for extra feature used in the program. Please note, the .gitlab-ci.yml has been commented out as it is listed as #.gitlab-ci.yml. This feature was used to have gitlab upload our backend API using docker+GCP. Due to the GCP free-trial expiring, we are simply using localhost:3000 on the frontend (React) and localhost:8080 on the backend using Node.js/Docker.





